package com.spring.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderABookApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderABookApplication.class, args);
	}

}
